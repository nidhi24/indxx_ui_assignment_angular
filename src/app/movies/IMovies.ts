export interface MovieElement {
  title: string;
  rt_score: number;
  director: string;
} 