export interface Movie {
  title: string;
  rt_score: number;
  director: string;
  producer: string;
  release_date: number;
  description: string;
}