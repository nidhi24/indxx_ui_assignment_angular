import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { MovieElement } from './IMovies';
import { Movie } from './IMovie';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppError } from '../app.error';
import { NotFoundError } from '../not-found-error';

@Injectable()
export class MovieService {
  private url = `https://ghibliapi.herokuapp.com/films`;
  constructor(private http: HttpClient) { }  
 
  /** GET movies from the server */
  getMovies() {
  return this.http.get<MovieElement[]>(this.url)
  .pipe(catchError((error: Response ) => this.handleError(error))) ;
 }

  getMovie(id) {
    return this.http.get<Movie>(`${this.url}/${id}`)
    .pipe(catchError((error:  Response ) => this.handleError(error))) ;
  }

  handleError(error){
     if (error.status === 404) {
        return throwError(new NotFoundError());
      } else {
        return throwError(new AppError(error));
      }
  }
}
