import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, MatDialog , MatSnackBar} from '@angular/material';
import { MovieDetailComponent } from '../movie-detail/movie-detail.component';
import { MovieElement } from '../IMovies';
import { Movie } from '../IMovie';
import { MovieService } from '../movie.service';
import { NotFoundError } from 'src/app/not-found-error';
import { AppError } from 'src/app/app.error';
import { SnackBarService } from 'src/app/snckBar.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  movieData: MovieElement[]=[];
  isDialogLoading = false;
  displayedColumns: string[] = ['title', 'director', 'rating'];
  dataSource;
  isError:boolean;
  isDialogErr=false;
  isLoading = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() { 
    this.isLoading=true;
    this.isError=false;
    this.ms.getMovies().subscribe((res) => {
      this.isLoading=false;
      this.movieData = res; 
      if(this.movieData.length===0){
        // No Data Found
        this.ds.displaySnackBar('Oops!! No Movie Found'); 
      } else {
        this.dataSource = new MatTableDataSource<MovieElement>(this.movieData);
        this.dataSource.paginator = this.paginator;
      }
     
     },(error : AppError)=>{
       this.isError=true;
       if(error instanceof NotFoundError){
        this.ds.displaySnackBar('Not Found'); 
       }
      else{
        this.ds.displaySnackBar('An Unexpected Error Occurred');
      }
     });
  }
  constructor(public dialog: MatDialog, private ms: MovieService,private ds: SnackBarService) {
  }
  openDialog(id) {
    this.isDialogLoading = true;
    this.ms.getMovie(id).subscribe((movie) => {
      const movieData: Movie = movie;
      this.dialog.open(MovieDetailComponent , {
        width: '600px',
        // tslint:disable-next-line: deprecation
        data: movieData } ).afterOpen().subscribe(() => {
          this.isDialogLoading = false;
        });
    }, (error : AppError)=>{
      if(error instanceof NotFoundError)
      {
        this.isDialogErr = true;
        this.ds.displaySnackBar('Not Found');
      }
      else{
        this.ds.displaySnackBar('An Unexpected Error Occurred');
      }
      
    });
  }

 

}
