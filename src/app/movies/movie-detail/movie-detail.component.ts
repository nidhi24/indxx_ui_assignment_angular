import {Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA} from '@angular/material';
import { Movie } from '../IMovie';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  movie: Movie;
  constructor(@Inject(MAT_DIALOG_DATA) public wrapMovie: any) {
  this.movie = wrapMovie;
  }
  ngOnInit() {
  }

}
