import { NgModule } from '@angular/core';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MovieComponent } from './movie/movie.component';
import {FormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angularMaterial.model';
@NgModule({
 declarations: [
  MovieDetailComponent, MovieComponent
  ],
  imports : [ FormsModule , CommonModule , AngularMaterialModule ],
  entryComponents: [ MovieDetailComponent ],
  exports: [ MovieComponent ]
})
export class MoviesModule {}
