import { NgModule } from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
 imports: [
    MatTableModule , MatDialogModule, MatSnackBarModule ,
     MatPaginatorModule , MatButtonModule , MatProgressSpinnerModule , BrowserAnimationsModule
  ],
  exports: [ MatTableModule , MatDialogModule, MatSnackBarModule ,
    MatPaginatorModule , MatButtonModule , MatProgressSpinnerModule , BrowserAnimationsModule ]
})
export class AngularMaterialModule {}
