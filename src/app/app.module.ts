import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from './shared.module';
import { MoviesModule } from './movies/movies.module';

@NgModule({
  declarations: [
    AppComponent,  HeaderComponent
  ],
  imports: [
    BrowserModule , SharedModule , MoviesModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
