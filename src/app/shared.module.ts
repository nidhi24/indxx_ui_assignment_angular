import { NgModule, ErrorHandler } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { MovieService } from './movies/movie.service';
import { HttpClientModule } from '@angular/common/http';
import { AppErrorHandler } from './AppErrorHandler';

@NgModule({
  imports:[ HttpClientModule ],
  providers: [MovieService ,
    {provide : MAT_DIALOG_DATA, useValue: {}},
    {provide: ErrorHandler , useClass: AppErrorHandler}
   ]
})
export class SharedModule {}
