import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {}

   displaySnackBar(message)
  {
      this.snackBar.open(message , 'close', {
        duration: 3000,
        horizontalPosition : 'center',
        verticalPosition : 'top'
      });
  }

}
